<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware'=> ['web' => 'auth']],function (){


	Route::resource('product','product');
Route::resource('category','cate');
	Route::get('home',function(){
if(Auth::user()->admin == 0){
	return view('home');
}else{
	$user=\App\User::all();
return 	 view('admin.index', compact('user'));

}
	});
		});

Route::get('order/{or}', 'HomeController@ordera');






