<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pro;
use Image;
 use App\category;
use Illuminate\Support\Facades\Auth;

class product extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->admin == 1){
        $products=pro::paginate(5);
        
        return view('admin.product.index',compact("products"));}

        else 
        {
            return view('error404');

                }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                if(Auth::user()->admin == 1){

        $cat=category::pluck('name','id');
        return view('admin.product.create',compact('cat'));
        }
        else 
        {
            return view('error404');

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
'pro_name'=>'required',
'pro_code'=>'required',
'pro_price'=>'required',
'pro_info'=>'required',
'spl_price'=>'required',
'image'=>'required|mimes:jpg,png,jpeg|max:1000',

        ]);
        $pro=new pro();
            $pro->pro_name=$request->input('pro_name');

            $pro->pro_code=$request->input('pro_code');

            $pro->pro_price=$request->input('pro_price');

            $pro->pro_info=$request->input('pro_info');
            $pro->stook=$request->input('stook');

            $pro->categories_id=$request->input('categories_id');
            $pro->spl_price=$request->input('spl_price');
           

        if($request->hasFile('image')){
          $img=$request->image;
    $filename=time() ."_".$img->getClientOriginalName();
    $localtion=public_path('image/'.$filename);
      //$img->move($localtion);
      image::make($img)->resize(1000,1000)->save($localtion);
     
      }

      $pro->image=$filename; 

    $pro->save();
    return redirect('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {                if(Auth::user()->admin == 1){

        $products=pro::find($id);
        return view('admin.product.edit',compact('products'));
   }
        else 
        {
            return view('error404');

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
   if(Auth::user()->admin == 1){
        $productdelete=pro::find($id);   
        unlink('image/'.$productdelete->image);

      $productdelete->delete();
      return back();;   
    } 

        else 
        {
            return view('error404');

        }
}

}