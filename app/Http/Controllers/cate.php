<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use Illuminate\Support\Facades\Auth;

class cate extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
    	        if(Auth::user()->admin == 1){

        $categorys=category::all();
        return view('admin.category.index',compact('categorys'));
     }
      else 
        {
            return view('error404');

                }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	    	        if(Auth::user()->admin == 1){

        return view('admin.category.creat');
          }
      else 
        {
            return view('error404');

                }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
"name"=>"required",
  ]);
        $pro=new category();
        $pro->name=$request->input('name');
        $pro->save();
        return redirect('home') ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	    	    	        if(Auth::user()->admin == 1){

          $categories=category::find($id);   

      $categories->delete();
      return back();  
  }
      else 
        {
            return view('error404');

                }
    }
}
